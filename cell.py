from Protein import Protein
import random as rand
from numpy import sqrt, sin, cos
from scipy.spatial import distance
#import matplotlib.pyplot as plt

def simulate(h, w, A, B, D, R, eps, P, dt, t):
    rand.seed()    
    a = []
    b = []
    bonds = []
    a = genParticles(h, w / 10, A)
    b = genParticles(h, w, B)

    for i in range(t):
        numBonds = 0
        for part in a:
            moveParticle(h, w / 10, part, D, dt)
        for part in b:
            moveParticle(h, w, part, D, dt)
        for part in a:
            numBonds += bondParticle(part, b, R, eps, P)
        for part in a:
            if part.bond != 0:
                part.R -= 1
        bonds.append(numBonds)
        # Progress
        #if i % 100 == 0:
        #    print(i)

    return bonds

# Parameters: h/w: height/width of placing area
#             n: number of particles to be generated
def genParticles(h, w, n):
    parts = []
    for i in range(n):
        parts.append(Protein(rand.uniform(0.0, w), rand.uniform(0.0, h), 0, 0))
    return parts

# Parameters: h/w: height/width of cell
#             part: particle to move
#             D: Coefficient of diffustion
#             dt: duration of time step
def moveParticle(h, w, part, D, dt):
    if part.bond == 0:
        dist = sqrt(4 * dt * D)
        theta = rand.randint(0, 359)
        deltaX = dist * sin(theta)
        deltaY = dist * cos(theta)

        while deltaX != 0 or deltaY != 0:
            partX = part.x
            partY = part.y
            if partX + deltaX < 0:
                part.x = 0
                deltaX += partX
                deltaX *= -1
            elif partX + deltaX > w:
                part.x = w
                deltaX -= w - partX
                deltaX *= -1
            else:
                part.x += deltaX
                deltaX = 0
            if partY + deltaY < 0:
                part.y = 0
                deltaY += partY
                deltaY *= -1
            elif partY + deltaY > h:
                part.y = h
                deltaY -= h - partY
                deltaY *= -1
            else:
                part.y += deltaY
                deltaY = 0

# Parameters: A: a single particle to be bonded
#             B: array of particles, bonding candidates
#             R: resident time
#             eps: epsilon
#             P: probability of bonding
def bondParticle(A, B, R, eps, P):
    if A.bond != 0:
        return 1
    
    aXY = A.xyToArr()
    for part in B:
        if distance.euclidean(aXY, part.xyToArr()) < eps:
            # Lock here
            if part.bond == 0 and rand.random() < P:
                A.bond = part
                A.R = R
                return 1
            # Unlock here
    return 0
