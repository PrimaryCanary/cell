import unittest
import random as rand
import cell

class CellTestCase(unittest.TestCase):
    rand.seed()
    
    def testMoveParticlesInside(self):
        h = rand.randint(1, 10) * 1000
        w = rand.randint(1, 10) * 1000
        A = rand.randint(1, 500)
        B = rand.randint(1, 500)
        D = rand.randint(1, 100) * 1000
        dt = rand.randint(1, 100)
        t = rand.randint(1, 500)
        a = cell.genParticles(h, w, A)
        b = cell.genParticles(h, w, B)
        
        aInside = True
        for part in a:
            if part.x < 0 or part.x > w or part.y < 0 or part.y > h:
                aInside = False
                break
        self.assertTrue(aInside)

        bInside = True
        for i in range(t):
            for part in b:
                if part.x < 0 or part.x > w or part.y < 0 or part.y > h:
                    bInside = False
                    break
            cell.moveParticles(h, w, b, D, dt)
        self.assertTrue(bInside)

    def testGenParticlesInside(self):
        h = rand.randint(1, 10) * 1000
        w = rand.randint(1, 10) * 1000
        n = rand.randint(1, 3000)
        parts = cell.genParticles(h, w, n)

        inside = True
        for p in parts:
            if p.x < 0 or p.x > w or p.y < 0 or p.y > h:
                inside = False
                break
        self.assertTrue(inside)

    def testGenParticlesN(self):
        h = rand.randint(1, 10) * 1000
        w = rand.randint(1, 10) * 1000
        n = rand.randint(1, 3000)
        parts = cell.genParticles(h, w, n)
        self.assertEqual(len(parts), n)

    def testBondParticlesN(self):
        h = rand.randint(1, 10) * 1000
        w = rand.randint(1, 10) * 1000
        A = rand.randint(1, 500)
        B = rand.randint(1, 500)
        R = rand.randint(0, 10)
        eps = rand.uniform(0, .1) * 1000
        P = rand.random()
        a = cell.genParticles(h, w, A)
        b = cell.genParticles(h, w, B)

        n = cell.bondParticles(a, b, R, eps, P)
        def f(x):
            return x.bond != 0
        testN = len(list(filter(f, a)))
        self.assertEqual(n, testN)

    def testBondParticlesFields(self):
        h = rand.randint(1, 10) * 1000
        w = rand.randint(1, 10) * 1000
        A = rand.randint(1, 500)
        B = rand.randint(1, 500)
        R = rand.randint(0, 10)
        eps = rand.uniform(0, .1) * 1000
        P = rand.random()
        a = cell.genParticles(h, w, A)
        b = cell.genParticles(h, w, B)

        cell.bondParticles(a, b, R, eps, P)
        res = True
        for part in a:
            if (part.bond != 0 and part.xyToArr() != part.bond.xyToArr()
                               and part.R != part.bond.R
                               and part.bond.bond != self):
                res = False
                break
        self.assertTrue(res)
                

if __name__ == '__main__':
    unittest.main()
