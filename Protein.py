class Protein:
    def __init__(self, x, y, bond, R):
        self._x = x
        self._y = y
        self._bond = bond # 0 if not bonded, Protein object otherwise
        self._R = R # 0 if not bonded, resident time remaining otherwise

    @property
    def x(self):
        return self._x
    @x.setter
    def x(self, x):
        self._x = x
        # Ensure bond stays at same x coord
        if self.bond != 0 and self.bond.x != x:
            self._bond._x = x

    @property
    def y(self):
        return self._y
    @y.setter
    def y(self, y):
        self._y = y
        # Ensure bond stays at same y coord
        if self.bond != 0 and self.bond.y != self.y:
            self._bond._y = y
        
    @property
    def bond(self):
        return self._bond
    @bond.setter
    def bond(self, p):
        # Ensure bond partner stays at same coords and is bonded to partner
        if p != 0:
            self._bond = p
            if self.bond != self:
                self._bond._bond = self
            if self.bond.x != self.x:
                self._bond._x = self.x
            if self.bond.y != self.y:
                self._bond._y = self.y
            if self.bond.R != self.R:
                self._bond._R = self.R
        # Break bond
        else:
            self._bond._bond = 0
            self._bond = 0

    @property
    def R(self):
        return self._R
    @R.setter
    def R(self, R):
        self._R = R
        # Ensure bond stays at same R
        if self.bond != 0 and self.bond.R != self.R:
            self._bond._R = R
        # Break bond at R = 0
        if R == 0:
            self.bond = 0
           
    def xyToArr(self):
        return [self.x, self.y]
