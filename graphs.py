from cell import simulate
from matplotlib import pyplot as plt
import numpy as np
from multiprocessing import Pool

def mean(arr):
    sum = 0
    for i in arr:
        sum += i
    return sum / len(arr)

if __name__ == '__main__':
    with Pool(processes=8) as pool:
        i = 0
        R = [1, 5, 50]
        P = [.1, .5, 1]
        eps = np.linspace(.01, .5, 50)

        params = {'H': .8, 'W': 3.8, 'A': 1000, 'B': 1000, 'D': 10, 'R': 20, 'eps': .1, 'P': .5, 'dt': .00025, 't': 500}
        for r in R:
            params['R'] = r
            for p in P:
                params['P'] = p
                y = []
                for e in eps:
                    params['eps'] = e
                    trials = [pool.apply_async(simulate, list(params.values())) for i in range(50)]
                    tmp = [res.get().pop() for res in trials]
                    y.append(mean(tmp))
                    print(e)

                i += 1
                print(i)
                plt.plot(eps, y)
                plt.xlabel('Epsilon')
                plt.ylabel('Average number of AB at steady state')
                plt.title(str(params))
                plt.savefig("./heterogeneous/R={},P={}.eps".format(r, p),format='eps', bbox_inches='tight')
                plt.clf()

